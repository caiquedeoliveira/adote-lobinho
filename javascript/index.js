function wolfCard(wolf){
    const wolves = document.querySelector('.wolves-list')
    const li = document.createElement('li')

    li.innerHTML = `
        <div class="wolf-image" style="background-image: url(${wolf.image_url});"></div>
        <div class="wolf-text">
            <h3 class="wolf-name">${wolf.name}</h3>
            <p class="wolf-age">Idade: ${wolf.age} anos</p>
            <p class="wolf-description">${wolf.description}</p> 
        </div>
    `

    wolves.appendChild(li)
}

let url = "https://lobinhos.herokuapp.com/wolves"

const getWolves = () => {
    fetch(url)
    .then(res => res.json())
    .then(wolves => {
        wolfCard(wolves[0])
        wolfCard(wolves[1])  
    })
    .catch(err => {
        console.log(err)
    })
}


getWolves()
