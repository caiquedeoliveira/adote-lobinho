let url = "https://lobinhos.herokuapp.com/wolves/"

const params  = new URLSearchParams(window.location.search)
const wolfId = params.get('id')
console.log(wolfId)

function adopt(wolf){
    const container = document.querySelector('.wolf-adoption')
    const div = document.createElement('div')

    div.innerHTML = `
    <div class="wolf-for-adoption">
        <div class="wolf-specific">
            <div class="round-image">
                <div class="wolf-image" style="background-image: url(${wolf.image_url});"></div>
            </div>

            <div class="wolf-title">
                <h1>Adote o(a) ${wolf.name}</h1>
                <p>ID:${wolf.id}</p>
            </div>
        </div>
    </div>
    `

    container.appendChild(div)
}




function getMyWolf(id){
    fetch(url)
    .then(res => res.json())
    .then(wolves => {
        for (let wolf of wolves){
            if(wolf.id == id){
                adopt(wolf)
            }
        }
})
}

getMyWolf(wolfId)


function addAdoptionButton(){
    const containerButton = document.querySelector('.adopt-button')
    const div = document.createElement('div')
    
    div.innerHTML = `
        <button onclick="adoptMyWolf(${wolfId})">Adotar</button>
    `
    containerButton.appendChild(div)
}
addAdoptionButton()



function adoptMyWolf(id){
    const adopterName = document.querySelector('#adopter-name').value
    const adopterAge = document.querySelector('#adopter-age').value
    const adopterEmail = document.querySelector('#adopter-email').value

    if(adopterName != "" && adopterAge != "" && adopterEmail != ""){
        const body = {
            wolf: {
                "adopter_name": adopterName,
                "adopter_age": adopterAge,
                "adopter_email": adopterEmail,
                "adopted":true
            }
        }
    
        const config = {
            method: "PUT",
            body: JSON.stringify(body),
            headers: {"Content-Type": "application/json"}
        }
    
        fetch(url + id, config)
        .then(res => res.json())
        .then(wolf => {
            console.log(wolf)
            alert("Lobo adotado! Atualizamos as informações.")
        })
        .catch(err => {
            console.log(err)
        })
    } else {
        alert('Confira os dados digitados.')
    }


    
}