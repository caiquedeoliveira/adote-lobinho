const url = "https://lobinhos.herokuapp.com/wolves"

function addWolf(){
    const name = document.querySelector('#name').value
    const age = document.querySelector('#age').value
    const image = document.querySelector('#image').value
    const description = document.querySelector('#description').value

    const nameReq = name.length >= 4 && name.length <= 60
    const ageReq = parseInt(age) > 0 && parseInt(age) <= 100
    const imageReq = ""
    const descriptReq = description.length >= 10 && description.length <= 255

    if (nameReq && ageReq && !imageReq && descriptReq){
        const body = {
            wolf: {
                "name": name,
                "age": age,
                "image_url": image,
                "description": description
            }
        }
    
        let config = {
            method: "POST",
            body: JSON.stringify(body),
            headers: {"Content-Type": "application/json"}   
        }
    
        fetch(url, config)
        .then(res => res.json())
        .then(wolf => {
            console.log(wolf)
            alert("Lobo adicionado")
        })
        .catch(err => {
            console.log(err)
        })
    } else {
        alert('Confira os dados digitados.')
    }

    
}