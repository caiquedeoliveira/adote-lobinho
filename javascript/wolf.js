let url = "https://lobinhos.herokuapp.com/wolves/"

const params  = new URLSearchParams(window.location.search)
const wolfId = params.get('id')
console.log(wolfId)


function singleWolf(wolf){
    const main = document.querySelector('main')
    const section = document.createElement('section')

    section.innerHTML = `
        <div class="single-wolf">
            <h1 class="wolf-title">${wolf.name}</h1>
            <div class="wolf-info">
                <div class="image-buttons">
                    <div>
                        <figure><img src="${wolf.image_url}"></figure>
                    </div>
                    <div class="wolf-buttons">
                        <button type="button" onclick="location.href='adopt.html?id=${wolfId}'">ADOTAR</button>
                        <button type="button" class="delete" onclick="deleteWolf(${wolfId})">EXCLUIR</button>
                    </div>
                </div>

                <div class="twop">
                    <p class="paragrafo">${wolf.description}</p>
                    <p class="paragrafo">${wolf.description}</p>
                </div>
            </div> 
        </div>
    `

    main.appendChild(section)
}


function getMyWolf(id){
    fetch(url)
    .then(res => res.json())
    .then(wolves => {
        for (let wolf of wolves){
            if(wolf.id == id){
                singleWolf(wolf)
            }
        }
})
}

getMyWolf(wolfId)


const deleteWolf = (id) => {
    const config = {
        method: "DELETE"
    }

    fetch(url+id, config)
    .then( () => {
        alert("Lobo apagado")
    })
    .catch(err => {
        console.log(err)
    })
}
