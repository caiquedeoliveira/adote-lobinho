let url = "https://lobinhos.herokuapp.com/wolves"

/* CARD DE LOBOS */
function wolfCard(wolf){
    const wolves = document.querySelector('.wolves-list')
    const li = document.createElement('li')

    li.innerHTML = `
        <div class="wolf-image" style="background-image: url(${wolf.image_url});"></div>
        <div class="wolf-text">
            <div class="wolf-info">
                <div class="name-age">
                    <h3 class="wolf-name">${wolf.name}</h3>
                    <p class="wolf-age">Idade: ${wolf.age} anos</p>
                </div>
                <div class="adopt-button">
                    <button onclick="location.href= 'wolf.html?id=${wolf.id}'">Adotar</button>
                </div>
            </div>
            <p class="wolf-description">${wolf.description}</p> 
        </div>
    `
    wolves.appendChild(li)
}


const getWolves = () => {
    fetch(url)
    .then(res => res.json())
    .then(wolves => {
        for (let wolf of wolves){
            wolfCard(wolf)
        }
    })
    .catch(err => {
        console.log(err)
    })
}

getWolves()



/* LOBOS ADOTADOS */
function adoptedWolves(wolf){
    const wolves = document.querySelector('.wolves-list')
    const li = document.createElement('li')

    li.innerHTML = `
        <div class="wolf-image" style="background-image: url(${wolf.image_url});"></div>
        <div class="wolf-text">
            <div class="wolf-info">
                <div class="name-age">
                    <h3 class="wolf-name">${wolf.name}</h3>
                    <p class="wolf-age">Idade: ${wolf.age} anos</p>
                </div>
                <div class="adopt-button">
                    <button style="background-color: #7AAC3A">Adotado</button>
                </div>
            </div>
            <p class="wolf-description">${wolf.description}</p> 
            <p class="wolf-owner">Adotado por: ${wolf.adopter_name}</p>
        </div>
    `
    wolves.append(li)
}

/* CHECKBOX */
const checkbox = document.querySelector('.filter')

checkbox.addEventListener("click", () => {
    if(checkbox.checked){
        document.querySelector('.wolves-list').innerHTML = ""
        let urladopted = "https://lobinhos.herokuapp.com/wolves/adopted/"
        fetch(urladopted)
        .then(res => res.json())
        .then(wolves => {
            for (let wolf of wolves){
                adoptedWolves(wolf)
            }
        })        
        .catch(err => {
            console.log(err)
        })
    } else {
        document.querySelector('.wolves-list').innerHTML = ""
        let url = "https://lobinhos.herokuapp.com/wolves/"
        fetch(url)
        .then(res => res.json())
        .then(wolves => {
            for (let wolf of wolves){
                wolfCard(wolf)
            }
        })
        .catch(err => {
            console.log(err)
        })
    }
})


/* FILTERING */
const searching = document.querySelector('.search-wolf input')
searching.addEventListener('keyup', el => {
        if (el.key === "Enter" && checkbox.checked){
            document.querySelector('.wolves-list').innerHTML = ""
            let url = "https://lobinhos.herokuapp.com/wolves/adopted/"
            fetch(url)
            .then(res => res.json())
            .then(wolves => {
                for (let wolf of wolves){
                    if (wolf.name.toLowerCase() == searching.value.toLowerCase()){
                        adoptedWolves(wolf)
                    }
                }
            })        
            .catch(err => {
                console.log(err)
            })
        } else {
            document.querySelector('.wolves-list').innerHTML = ""
            let url = "https://lobinhos.herokuapp.com/wolves/"
            fetch(url)
            .then(res => res.json())
            .then(wolves => {
                for (let wolf of wolves){
                    if (wolf.name.toLowerCase() == searching.value.toLowerCase()){
                        wolfCard(wolf)
                    }
                }
            })        
            .catch(err => {
                console.log(err)
            })
        }
})



/* PAGINAÇÃO */



function pagination(total){
    const ul = document.querySelector('.pagination')
    for(let i=0; i<total; i++){
        let li = document.createElement('li')
        li.innerHTML = `<a href="#">${i+1}</a>`
        ul.appendChild(li)
    }
    let li = document.createElement('li')
    li.innerHTML = `<a href="#">»</a>`
    ul.appendChild(li)
}

pagination(10) 









